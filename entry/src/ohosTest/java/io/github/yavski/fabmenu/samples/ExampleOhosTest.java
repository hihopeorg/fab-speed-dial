package io.github.yavski.fabmenu.samples;

import io.github.yavski.fabmenu.samples.util.EventHelper;
import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.FloatingActionButton;
import io.github.yavski.fabspeeddial.samples.EventsSampleActivity;
import io.github.yavski.fabspeeddial.samples.GroupStyleSampleActivity;
import io.github.yavski.fabspeeddial.samples.IndividualStyleSampleActivity;
import io.github.yavski.fabspeeddial.samples.MenuItemsSampleActivity;
import io.github.yavski.fabspeeddial.samples.PositionSampleActivity;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Switch;
import ohos.agp.components.TextField;
import ohos.agp.components.element.Element;
import org.junit.After;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ExampleOhosTest {
    @After
    public void tearDown() {
        EventHelper.clearAbilities();
    }

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("io.github.yavski.fabmenu.samples", actualBundleName);
    }

    @Test
    public void testPosition() {
        Ability ability = EventHelper.startAbility(PositionSampleActivity.class);
        EventHelper.sleep(5000);
        StackLayout contentView = (StackLayout) ability.findComponentById(ResourceTable.Id_content_view);
        try {
            Field fab = FabSpeedDial.class.getDeclaredField("fab");
            fab.setAccessible(true);

            FabSpeedDial bottomEnd = (FabSpeedDial) contentView.getComponentAt(0);
            FabSpeedDial bottomStart = (FabSpeedDial) contentView.getComponentAt(1);
            FabSpeedDial topStart = (FabSpeedDial) contentView.getComponentAt(2);
            FabSpeedDial topEnd = (FabSpeedDial) contentView.getComponentAt(3);
            EventHelper.triggerClickEvent(ability, (FloatingActionButton) fab.get(bottomEnd));
            EventHelper.triggerClickEvent(ability, (FloatingActionButton) fab.get(bottomStart));
            EventHelper.triggerClickEvent(ability, (FloatingActionButton) fab.get(topStart));
            EventHelper.triggerClickEvent(ability, (FloatingActionButton) fab.get(topEnd));
            EventHelper.sleep(2000);
            assertTrue("FabSpeedDial(bottomEnd) openMenu failed", bottomEnd.isMenuOpen());
            assertTrue("FabSpeedDial(bottomStart) openMenu failed", bottomStart.isMenuOpen());
            assertTrue("FabSpeedDial(topStart) openMenu failed", topStart.isMenuOpen());
            assertTrue("FabSpeedDial(topEnd) openMenu failed", topEnd.isMenuOpen());
            Method isGravityBottom = FabSpeedDial.class.getDeclaredMethod("isGravityBottom");
            isGravityBottom.setAccessible(true);
            Method isGravityEnd = FabSpeedDial.class.getDeclaredMethod("isGravityEnd");
            isGravityEnd.setAccessible(true);
            assertTrue("FabSpeedDial(bottomEnd)'s gravity is wrong",
                    (Boolean) isGravityBottom.invoke(bottomEnd) && (Boolean) isGravityEnd.invoke(bottomEnd));
            assertTrue("FabSpeedDial(bottomStart)'s gravity is wrong",
                    (Boolean) isGravityBottom.invoke(bottomStart) && !(Boolean) isGravityEnd.invoke(bottomStart));
            assertTrue("FabSpeedDial(topStart)'s gravity is wrong",
                    !(Boolean) isGravityBottom.invoke(topStart) && !(Boolean) isGravityEnd.invoke(topStart));
            assertTrue("FabSpeedDial(topEnd)'s gravity is wrong",
                    !(Boolean) isGravityBottom.invoke(topEnd) && (Boolean) isGravityEnd.invoke(topEnd));
        } catch (IllegalAccessException | NoSuchMethodException | NoSuchFieldException | InvocationTargetException e) {
            e.printStackTrace();
            assertTrue("reflection error " + e.toString(), false);
        }
    }


    @Test
    public void testStyle1() {
        Ability ability = EventHelper.startAbility(GroupStyleSampleActivity.class);

        EventHelper.sleep(5000);
        FabSpeedDial fabSpeedDial = (FabSpeedDial) ability.findComponentById(ResourceTable.Id_fab_speed);
        try {
            Field fab = FabSpeedDial.class.getDeclaredField("fab");
            fab.setAccessible(true);
            EventHelper.triggerClickEvent(ability, (FloatingActionButton) fab.get(fabSpeedDial));
            Field miniFabBackgroundTintF = FabSpeedDial.class.getDeclaredField("miniFabBackgroundTint");
            miniFabBackgroundTintF.setAccessible(true);
            Field miniFabDrawableTintF = FabSpeedDial.class.getDeclaredField("miniFabDrawableTint");
            miniFabBackgroundTintF.setAccessible(true);
            Field miniFabTitleTextColorF = FabSpeedDial.class.getDeclaredField("miniFabTitleTextColor");
            miniFabBackgroundTintF.setAccessible(true);
            Field touchGuardDrawableF = FabSpeedDial.class.getDeclaredField("touchGuardDrawable");
            miniFabBackgroundTintF.setAccessible(true);
            FabSpeedDial.ColorStateList miniFabBackgroundTintV =
                    (FabSpeedDial.ColorStateList) miniFabBackgroundTintF.get(fabSpeedDial);
            assertEquals("miniFabBackgroundTint color is different from xml", 0xFFFFFFFF,
                    miniFabBackgroundTintV.getDefaultColor());
            FabSpeedDial.ColorStateList miniFabDrawableTintV =
                    (FabSpeedDial.ColorStateList) miniFabDrawableTintF.get(fabSpeedDial);
            assertEquals("miniFabDrawableTint color is different from xml", 0xFF512DA8,
                    miniFabDrawableTintV.getDefaultColor());
            int miniFabTitleTextColorV = miniFabTitleTextColorF.getInt(fabSpeedDial);
            assertEquals("miniFabTitleTextColor is different from xml", 0xFF512DA8, miniFabTitleTextColorV);
            Element touchGuardDrawableV = (Element) touchGuardDrawableF.get(fabSpeedDial);
            assertNotNull("touchGuardDrawable is null", touchGuardDrawableV);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testTouchGuard() {
        Ability ability = EventHelper.startAbility(GroupStyleSampleActivity.class);
        EventHelper.sleep(5000);
        FabSpeedDial fabSpeedDial = (FabSpeedDial) ability.findComponentById(ResourceTable.Id_fab_speed);
        try {
            Field fab = FabSpeedDial.class.getDeclaredField("fab");
            fab.setAccessible(true);
            EventHelper.triggerClickEvent(ability, (FloatingActionButton) fab.get(fabSpeedDial));
            EventHelper.sleep(2000);
            assertTrue("FabSpeedDial open failed", fabSpeedDial.isMenuOpen());

            Field touchGuard = FabSpeedDial.class.getDeclaredField("touchGuard");
            EventHelper.triggerClickEvent(ability, (Component) touchGuard.get(fabSpeedDial));
            EventHelper.sleep(2000);
            assertFalse("touchGuard no use", fabSpeedDial.isMenuOpen());
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testStyle2() {
        Ability ability = EventHelper.startAbility(IndividualStyleSampleActivity.class);
        EventHelper.sleep(5000);
        FabSpeedDial fabSpeedDial = (FabSpeedDial) ability.findComponentById(ResourceTable.Id_fab_speed);
        try {
            Field fab = FabSpeedDial.class.getDeclaredField("fab");
            fab.setAccessible(true);
            EventHelper.triggerClickEvent(ability, (FloatingActionButton) fab.get(fabSpeedDial));
            Field miniFabBackgroundTintArrayF = FabSpeedDial.class.getDeclaredField("miniFabBackgroundTintArray");
            miniFabBackgroundTintArrayF.setAccessible(true);
            Field miniFabDrawableTintF = FabSpeedDial.class.getDeclaredField("miniFabDrawableTint");
            miniFabDrawableTintF.setAccessible(true);
            Field miniFabTitleTextColorListF = FabSpeedDial.class.getDeclaredField("miniFabTitleTextColorList");
            miniFabTitleTextColorListF.setAccessible(true);

            int[] miniFabBackgroundTintArrayV = (int[]) miniFabBackgroundTintArrayF.get(fabSpeedDial);

            assertArrayEquals("miniFabBackgroundTintArray color is different from xml",
                    new int[]{-3407872,
                            -5609780,
                            -6697984}, miniFabBackgroundTintArrayV);
            FabSpeedDial.ColorStateList miniFabDrawableTintV =
                    (FabSpeedDial.ColorStateList) miniFabDrawableTintF.get(fabSpeedDial);
            assertEquals("miniFabDrawableTint color is different from xml", 0xFFFFFFFF,
                    miniFabDrawableTintV.getDefaultColor());
            int[] miniFabTitleTextColorListV = (int[]) miniFabTitleTextColorListF.get(fabSpeedDial);
            assertArrayEquals("miniFabTitleTextColorList color is different from xml",
                    new int[]{-3407872,
                            -5609780,
                            -6697984}, miniFabTitleTextColorListV);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCallback() {
        Ability ability = EventHelper.startAbility(EventsSampleActivity.class);
        EventHelper.sleep(5000);
        FabSpeedDial fabSpeedDial = (FabSpeedDial) ability.findComponentById(ResourceTable.Id_fab_speed_dial);
        try {
            Field fab = FabSpeedDial.class.getDeclaredField("fab");
            fab.setAccessible(true);
            EventHelper.triggerClickEvent(ability, (FloatingActionButton) fab.get(fabSpeedDial));
            EventHelper.sleep(2000);
            assertTrue("FabSpeedDial open failed", fabSpeedDial.isMenuOpen());
            DirectionalLayout menuItemsLayout =
                    (DirectionalLayout) fabSpeedDial.findComponentById(io.github.yavski.fabspeeddial.ResourceTable.Id_menu_items_layout);
            DirectionalLayout menuItem0 = (DirectionalLayout) menuItemsLayout.getComponentAt(0);
            FloatingActionButton miniFab0 =
                    (FloatingActionButton) menuItem0.findComponentById(ResourceTable.Id_mini_fab);
            EventHelper.triggerClickEvent(ability, miniFab0);
            EventHelper.sleep(2000);
            assertFalse("FabSpeedDial callback close failed", fabSpeedDial.isMenuOpen());
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testMenuItem() {
        Ability ability = EventHelper.startAbility(MenuItemsSampleActivity.class);
        EventHelper.sleep(5000);
        FabSpeedDial fabSpeedDial = (FabSpeedDial) ability.findComponentById(ResourceTable.Id_fab_speed_dial);
        TextField inputView = (TextField) ability.findComponentById(ResourceTable.Id_puppy_name);
        EventHelper.postOnUiThread(() -> {
            inputView.setText("fido");
        });
        Switch switch0 = (Switch) ability.findComponentById(ResourceTable.Id_menu_item_call_switch);
        EventHelper.triggerClickEvent(ability, switch0);
        try {
            Field fab = FabSpeedDial.class.getDeclaredField("fab");
            fab.setAccessible(true);
            EventHelper.triggerClickEvent(ability, (FloatingActionButton) fab.get(fabSpeedDial));
            EventHelper.sleep(2000);
            assertTrue("FabSpeedDial open failed", fabSpeedDial.isMenuOpen());
            DirectionalLayout menuItemsLayout =
                    (DirectionalLayout) fabSpeedDial.findComponentById(io.github.yavski.fabspeeddial.ResourceTable.Id_menu_items_layout);
            DirectionalLayout menuItem0 = (DirectionalLayout) menuItemsLayout.getComponentAt(0);
            assertEquals("menuItem count not right", 2, menuItemsLayout.getChildCount());
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}