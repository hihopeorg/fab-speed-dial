package io.github.yavski.fabspeeddial;

import junit.framework.TestCase;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Objects;

public class SimpleMenuListenerAdapterTest extends TestCase {

    public void testOnPrepareMenu() {
        Class clz = SimpleMenuListenerAdapter.class;
        try {
            Method method = clz.getMethod("onPrepareMenu", NavigationMenu.class);
            int modifiers = method.getModifiers();
            assertFalse("onPrepareMenu is abstract!", Modifier.isAbstract(modifiers));
            assertTrue("return type is not correct!", Objects.equals(method.getReturnType(), boolean.class));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            assertTrue("can not find method onPrepareMenu(NavigationMenu)", false);
        }
    }

    public void testOnPrepareMenu2() {
        SimpleMenuListenerAdapter simpleMenuListenerAdapter = new SimpleMenuListenerAdapter();
        boolean defaultResult = simpleMenuListenerAdapter.onPrepareMenu(null);
        assertTrue("SimpleMenuListenerAdapter.onPrepareMenu should return true by default!", defaultResult);
    }

    public void testOnMenuItemSelected() {
        Class clz = SimpleMenuListenerAdapter.class;
        try {
            Method method = clz.getMethod("onMenuItemSelected", MenuItem.class);
            int modifiers = method.getModifiers();
            assertFalse("onMenuItemSelected is abstract!", Modifier.isAbstract(modifiers));
            assertTrue("return type is not correct!", Objects.equals(method.getReturnType(), boolean.class));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            assertTrue("can not find method onMenuItemSelected(MenuItem)", false);
        }
    }

    public void testOnMenuItemSelected2() {
        SimpleMenuListenerAdapter simpleMenuListenerAdapter = new SimpleMenuListenerAdapter();
        boolean defaultResult = simpleMenuListenerAdapter.onMenuItemSelected(null);
        assertFalse("SimpleMenuListenerAdapter.onMenuItemSelected should return false by default!", defaultResult);
    }

    public void testOnMenuClosed() {
        Class clz = SimpleMenuListenerAdapter.class;
        try {
            Method method = clz.getMethod("onMenuClosed");
            int modifiers = method.getModifiers();
            assertFalse("onMenuClosed is abstract!", Modifier.isAbstract(modifiers));
            assertTrue("return type is not correct!", Objects.equals(method.getReturnType(), void.class));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            assertTrue("can not find method onMenuClosed()", false);
        }
    }
}