package io.github.yavski.fabspeeddial;

import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;

import java.lang.reflect.Field;
import java.util.Objects;

public class FabSpeedDialTest extends TestCase {

    public void testSetMenuListener() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        FabSpeedDial fabSpeedDial = new FabSpeedDial(context);
        FabSpeedDial.MenuListener menuListener = new SimpleMenuListenerAdapter();
        fabSpeedDial.setMenuListener(menuListener);
        try {
            Field field = FabSpeedDial.class.getDeclaredField("menuListener");
            field.setAccessible(true);
            Object obj = field.get(fabSpeedDial);
            assertTrue("setMenuListener failed, field not be modified", Objects.equals(menuListener, obj));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
            assertTrue("can not find field menuListener",false);
        }
    }

    public void testIsMenuOpen() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        FabSpeedDial fabSpeedDial = new FabSpeedDial(context);
        assertFalse("isMenuOpen should be false by default", fabSpeedDial.isMenuOpen());
        // The remain UI effect can not be tested without attachedToWindow, see UI test.
    }

    public void testOpenMenu() {
        // The UI effect can not be tested without attachedToWindow, see UI test.
    }

    public void testCloseMenu() {
        // The UI effect can not be tested without attachedToWindow, see UI test.
    }

    public void testShow() {
        // The UI effect can not be tested without attachedToWindow, see UI test.
    }

    public void testHide() {
        // The UI effect can not be tested without attachedToWindow, see UI test.
    }
}