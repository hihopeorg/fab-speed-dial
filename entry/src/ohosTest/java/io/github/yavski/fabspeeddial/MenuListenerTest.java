package io.github.yavski.fabspeeddial;
import junit.framework.TestCase;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Objects;

public class MenuListenerTest extends TestCase {

    public void testOnPrepareMenu() {
        Class clz = SimpleMenuListenerAdapter.class;
        try {
            Method method = clz.getMethod("onPrepareMenu", NavigationMenu.class);
            int modifiers = method.getModifiers();
            assertTrue("onPrepareMenu is not public!", Modifier.isPublic(modifiers));
            assertTrue("return type is not correct!", Objects.equals(method.getReturnType(), boolean.class));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            assertTrue("can not find method onPrepareMenu(NavigationMenu)", false);
        }
    }

    public void testOnMenuItemSelected() {
        Class clz = SimpleMenuListenerAdapter.class;
        try {
            Method method = clz.getMethod("onMenuItemSelected", MenuItem.class);
            int modifiers = method.getModifiers();
            assertTrue("onMenuItemSelected is not public!", Modifier.isPublic(modifiers));
            assertTrue("return type is not correct!", Objects.equals(method.getReturnType(), boolean.class));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            assertTrue("can not find method onMenuItemSelected(MenuItem)", false);
        }
    }


    public void testOnMenuClosed() {
        Class clz = SimpleMenuListenerAdapter.class;
        try {
            Method method = clz.getMethod("onMenuClosed");
            int modifiers = method.getModifiers();
            assertTrue("onMenuClosed is not public!", Modifier.isPublic(modifiers));
            assertTrue("return type is not correct!", Objects.equals(method.getReturnType(), void.class));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            assertTrue("can not find method onMenuClosed()", false);
        }
    }
}
