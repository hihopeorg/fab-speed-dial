/*
 * Copyright 2016 Yavor Ivanov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.yavski.fabspeeddial.samples;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import io.github.yavski.fabmenu.samples.ResourceTable;

public class HomeActivity extends Ability implements ListContainer.ItemClickedListener {

    private String[] titles;

    @Override
    protected void onStart(Intent  intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_home);

        titles = getStringArray(ResourceTable.Strarray_title_array);
        final String[] descriptions = getStringArray(ResourceTable.Strarray_description_array);

        BaseItemProvider adapter = new BaseItemProvider(){

            @Override
            public int getCount() {
                return titles.length;
            }

            @Override
            public Object getItem(int i) {
                return null;
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
                if (component == null) {
                    component = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_simple_list_item_2,null,false);
                }

                Text text1 = (Text) component.findComponentById(ResourceTable.Id_text1);
                Text text2 = (Text) component.findComponentById(ResourceTable.Id_text2);

                text1.setText(titles[i]);
                text2.setText(descriptions[i]);
                return component;
            }
        };
        ListContainer listView = (ListContainer) findComponentById(ResourceTable.Id_list_view);
        listView.setItemProvider(adapter);
        listView.setItemClickedListener(this);
    }
    @Override
    public void onItemClicked(ListContainer parent, Component view, int position, long id) {
        final Class<? extends BaseSampleActivity> newActivity;
        switch (position) {
            default:
            case 0 :
                newActivity = PositionSampleActivity.class;
                break;
            case 1:
                newActivity = GroupStyleSampleActivity.class;
                break;
            case 2:
                newActivity = IndividualStyleSampleActivity.class;
                break;
            case 3:
                newActivity = EventsSampleActivity.class;
                break;
            case 4:
                newActivity = MenuItemsSampleActivity.class;
                break;
        }
        Intent i = new Intent();
        Operation operation = new Intent.OperationBuilder().withBundleName(getBundleName()).withAbilityName(newActivity).build();
        i.setOperation(operation);
        i.setParam(BaseSampleActivity.TITLE, titles[position]);
        startAbility(i);
    }
}
