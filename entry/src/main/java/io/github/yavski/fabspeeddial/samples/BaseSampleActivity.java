package io.github.yavski.fabspeeddial.samples;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * Created by yavorivanov on 03/01/2016.
 */
public abstract class BaseSampleActivity extends Ability {

    public static final String TITLE = "title";

    private String mTitle;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        mTitle = getIntent().getStringParam(TITLE);
    }
}
