/*
 * Copyright 2016 Yavor Ivanov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.yavski.fabspeeddial.samples;

import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.MenuItem;
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter;
import ohos.aafwk.content.Intent;
import ohos.agp.window.dialog.ToastDialog;
import io.github.yavski.fabmenu.samples.ResourceTable;

public class EventsSampleActivity extends BaseSampleActivity {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_events_sample);

        FabSpeedDial fabSpeedDial = (FabSpeedDial) findComponentById(ResourceTable.Id_fab_speed_dial);
        fabSpeedDial.setMenuListener(new SimpleMenuListenerAdapter() {
            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                new ToastDialog(getContext()).setText(getString(ResourceTable.String_selected_menu_item, menuItem.getTitle())).setDuration(2000).show();
                return false;
            }
        });
    }
}
