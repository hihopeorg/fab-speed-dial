/*
 * Copyright 2016 Yavor Ivanov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.yavski.fabspeeddial.samples;

import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.MenuItem;
import io.github.yavski.fabspeeddial.NavigationMenu;
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Switch;
import ohos.agp.components.TextField;
import ohos.agp.utils.TextTool;
import ohos.agp.window.dialog.ToastDialog;
import io.github.yavski.fabmenu.samples.ResourceTable;

public class MenuItemsSampleActivity extends BaseSampleActivity {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_menu_items_sample);

        final TextField inputView = (TextField) findComponentById(ResourceTable.Id_puppy_name);

        FabSpeedDial fabSpeedDial = ((FabSpeedDial) findComponentById(ResourceTable.Id_fab_speed_dial));
        fabSpeedDial.setMenuListener(new SimpleMenuListenerAdapter() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                String input = inputView.getText().toString();

                if (TextTool.isNullOrEmpty(input)) {
                    new ToastDialog(getContext()).setText("Enter the name of your puppy").setDuration(2000).show();
                    return false;
                }

                setMenuItemTitle(navigationMenu, ResourceTable.Id_action_call, input);
                setMenuItemTitle(navigationMenu, ResourceTable.Id_action_text, input);
                setMenuItemTitle(navigationMenu, ResourceTable.Id_action_email, input);

                setMenuItemVisibility(navigationMenu, ResourceTable.Id_action_call, ResourceTable.Id_menu_item_call_switch);
                setMenuItemVisibility(navigationMenu, ResourceTable.Id_action_text, ResourceTable.Id_menu_item_text_switch);
                setMenuItemVisibility(navigationMenu, ResourceTable.Id_action_email, ResourceTable.Id_menu_item_email_switch);

                return true;
            }
        });

    }

    private void setMenuItemTitle(NavigationMenu menu, int menuItemId, CharSequence input) {
        MenuItem menuItem = menu.findItem(menuItemId);
        String oldMenuItemTitle = menuItem.getTitle().toString();
        menuItem.setTitle(oldMenuItemTitle + " " + input);
    }

    private void setMenuItemVisibility(NavigationMenu menu, int menuItemId, int switchItem) {
        Switch switchView = (Switch) findComponentById(switchItem);
        menu.findItem(menuItemId).setVisible(switchView.isChecked());
    }

}
