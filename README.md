# fab-speed-dial

**本项目是基于开源项目fab-speed-dial进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/yavski/fab-speed-dial ）追踪到原项目版本**

#### 项目介绍

- 项目名称：fab-speed-dial自定义拨号菜单
- 所属系列：ohos的第三方组件适配移植
- 功能：提供一套自定义拨号菜单控件，可添加事件处理唤起拨号盘等服务
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/yavski/fab-speed-dial
- 原项目基线版本：未发布Release版本
- 编程语言：Java
- 外部库依赖：无

#### 效果展示

<img src="gif/fab_speed_dial1.gif"/>

<img src="gif/fab_speed_dial2.gif"/>

#### 安装教程

##### 方案一：

1. 下载fab-speed-dial三方库har包library.har。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。

3. 在module级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```groovy
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'library', ext: 'har')
	……
}
```
##### 方案二：

  1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:

```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/'
     }
 }
```

  2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
 dependencies {
     implementation 'io.github.yavski.ohos:fab-speed-dial:1.0.0'
 }
```

#### 使用说明

##### 自定义menu菜单 ohos目前使用自定义布局定义
```
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    xmlns:app="http://schemas.ohos.com/apk/res-auto"
    ohos:width="match_content"
    ohos:height="match_content"
    ohos:orientation="vertical">

    <io.github.yavski.fabspeeddial.MenuItem
        ohos:id="$+id:action_call"
        ohos:width="match_content"
        ohos:height="match_content"
        app:icon="$graphic:ic_call_black_24px"
        app:title="$string:menu_item_call"
        app:showAsAction="never"/>

    <io.github.yavski.fabspeeddial.MenuItem
        ohos:id="$+id:action_text"
        ohos:width="match_content"
        ohos:height="match_content"
        app:icon="$graphic:ic_chat_bubble_outline_black_24px"
        app:title="$string:menu_item_text"
        app:showAsAction="never"/>

    <io.github.yavski.fabspeeddial.MenuItem
        ohos:id="$+id:action_email"
        ohos:width="match_content"
        ohos:height="match_content"
        app:icon="$graphic:ic_mail_outline_black_24px"
        app:title="$string:menu_item_email"
        app:showAsAction="never"/>

</DirectionalLayout>
```

##### 将 FabSpeedDial 添加到你的布局 xml
```
<StackLayout xmlns:ohos="http://schemas.huawei.com/res/ohos"
    xmlns:app="http://schemas.ohos.com/apk/res-auto"
    ohos:width="match_parent"
    ohos:height="match_parent"
    ohos:padding="16vp">

    <io.github.yavski.fabspeeddial.FabSpeedDial
        ohos:width="match_content"
        ohos:height="match_content"
        ohos:layout_alignment="bottom|end"
        app:fabGravity="bottom_end"
        app:fabMenu="$layout:menu_main" />

</StackLayout>
```

##### 事件回调

通过onPrepareMenu和onMenuItemSelected处理自定义事件回调:

```java
        FabSpeedDial fabSpeedDial = (FabSpeedDial) findComponentById(ResourceTable.Id_fab_speed_dial);
        fabSpeedDial.setMenuListener(new SimpleMenuListenerAdapter() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                // TODO: Do something with yout menu items, or return false if you don't want to show them
                return true;
            }
        });
```

```java
        FabSpeedDial fabSpeedDial = (FabSpeedDial) findComponentById(ResourceTable.Id_fab_speed_dial);
        fabSpeedDial.setMenuListener(new SimpleMenuListenerAdapter() {
            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
               //TODO: Start some activity
              return false;
            }
        });
```

##### 自定义属性

| 自定义属性|描述|
| ------------- |-------------|
| app:fabDrawable | 主FAB背景 |
| app:fabDrawableTint | 主FAB色调 |
| app:fabBackgroundTint | 主FAB背景色调 |
| app:miniFabDrawableTint | 迷你FAB icon图标 |
| app:miniFabBackgroundTint | 迷你FAB icon 背景色 |
| app:miniFabBackgroundTintList | 迷你FAB按钮背景色 |
| app:miniFabTitleTextColor | 迷你FAB文字颜色 |
| app:miniFabTitleTextColorList | 迷你FAB按钮的背景色 |
| app:miniFabTitleBackgroundTint | 迷你FAB标题的背景色 |
| app:miniFabTitlesEnabled | 隐藏迷你FAB的标题 |
| app:touchGuard | 是否在点击外部时隐藏 |
| app:touchGuardDrawable | 设置FAB周边背景 |

#### 版本迭代


- v1.0.0


#### 版权和许可信息

```
<!--
  ~ Copyright 2016 Yavor Ivanov
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->
```
