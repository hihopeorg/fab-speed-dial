/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.yavski.fabspeeddial;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

public class CardView extends StackLayout implements Component.DrawTask {

    private Color cardBackgroundColor = new Color(0);
    private int cardCornerRadius = 0;

    public CardView(Context context) {
        super(context);
        init(null);
    }

    public CardView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    public CardView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    public void init(AttrSet attrSet) {
        if (attrSet != null) {
            cardBackgroundColor = ResourceHelper.getColor(attrSet, "cardBackgroundColor", cardBackgroundColor);
            cardCornerRadius = ResourceHelper.getDimension(attrSet, "cardCornerRadius", cardCornerRadius);
            setPaddingLeft(ResourceHelper.getDimension(attrSet, "contentPaddingLeft", 0));
            setPaddingTop(ResourceHelper.getDimension(attrSet, "contentPaddingTop", 0));
            setPaddingRight(ResourceHelper.getDimension(attrSet, "contentPaddingRight", 0));
            setPaddingBottom(ResourceHelper.getDimension(attrSet, "contentPaddingBottom", 0));
        }
        setClipEnabled(false);
        addDrawTask(this, DrawTask.BETWEEN_BACKGROUND_AND_CONTENT);
    }

    public void setCardBackgroundColor(int color) {
        cardBackgroundColor = new Color(color);
        getContext().getUITaskDispatcher().asyncDispatch(this::invalidate);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        drawBackground(component, canvas);
    }

    private void drawBackground(Component component, Canvas canvas) {
        float width = component.getWidth();
        float height = component.getHeight();
        float radius = Math.min(cardCornerRadius, Math.min(width / 2, height / 2));
        Path path = new Path();
        path.moveTo(radius, 0);
        path.lineTo(width - radius, 0);
        path.arcTo(new RectFloat(width - 2 * radius, 0, width, radius * 2), 270, 90);
        path.lineTo(width, height - radius);
        path.arcTo(new RectFloat(width - 2 * radius, height - 2 * radius, width, height), 0, 90);
        path.lineTo(radius, height);
        path.arcTo(new RectFloat(0, height - 2 * radius, radius * 2, height), 90, 90);
        path.lineTo(0, radius);
        path.arcTo(new RectFloat(0, 0, radius * 2, radius * 2), 180, 90);
        path.close();

        Paint backgroundPaint = new Paint();
        backgroundPaint.setAntiAlias(true);

        int saveCount = canvas.save();
        canvas.translate(0, AttrHelper.vp2px(1, getContext()));
        canvas.clipPath(path, Canvas.ClipOp.INTERSECT);
        backgroundPaint.setColor(new Color(0xff777777));
        canvas.drawRect(new RectFloat(0, 0, component.getWidth(), component.getHeight()), backgroundPaint);
        canvas.restoreToCount(saveCount);

        canvas.clipPath(path, Canvas.ClipOp.INTERSECT);
        backgroundPaint.setColor(cardBackgroundColor);
        canvas.drawRect(new RectFloat(0, 0, component.getWidth(), component.getHeight()), backgroundPaint);

    }
}
