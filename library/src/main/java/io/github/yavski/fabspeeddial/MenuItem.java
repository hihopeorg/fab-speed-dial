/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.yavski.fabspeeddial;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.app.Context;

public class MenuItem extends Text {
    private String title;
    private Element icon;
    private int order;

    public MenuItem(Context context) {
        super(context);
        init(null);
    }

    public MenuItem(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    public MenuItem(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    private void init(AttrSet attrSet) {
        if (attrSet != null) {
            title = ResourceHelper.getString(attrSet, "title", "");
            icon = ResourceHelper.getElement(attrSet, "icon");
            setTitle(title);
            setIcon(icon);
        }
    }

    public boolean isVisible() {
        return getVisibility() == VISIBLE;
    }

    public void setTitle(String title) {
        this.title = title;
        setText(title);
    }

    public String getTitle() {
        return title;
    }

    public void setIcon(Element icon) {
        this.icon = icon;
        setAroundElementsRelative(icon, null, null, null);
    }

    public Element getIcon() {
        return icon;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getOrder() {
        return order;
    }

    public int getMenuId() {
        return getId();
    }

    public void setVisible(boolean visible) {
        setVisibility(visible ? VISIBLE : HIDE);
    }
}
