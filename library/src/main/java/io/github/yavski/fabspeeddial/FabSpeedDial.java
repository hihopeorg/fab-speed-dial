/*
 * Copyright 2016 Yavor Ivanov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.yavski.fabspeeddial;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ComponentParent;
import ohos.agp.components.ComponentState;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.KeyEvent;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by yavorivanov on 01/01/2016.
 */
public class FabSpeedDial extends DirectionalLayout implements Component.ClickedListener,
        Component.BindStateChangedListener, Component.KeyEventListener {
    HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, FabSpeedDial.class.getSimpleName());

    /**
     * Called to notify of close and selection changes.
     */
    public interface MenuListener {

        /**
         * Called just before the menu items are about to become visible.
         * Don't block as it's called on the main thread.
         *
         * @param navigationMenu The menu containing all menu items.
         * @return You must return true for the menu to be displayed;
         * if you return false it will not be shown.
         */
        boolean onPrepareMenu(NavigationMenu navigationMenu);

        /**
         * Called when a menu item is selected.
         *
         * @param menuItem The menu item that is selected
         * @return whether the menu item selection was handled
         */
        boolean onMenuItemSelected(MenuItem menuItem);

        void onMenuClosed();
    }

    private static final int VSYNC_RHYTHM = 16;
    public static final int BOTTOM_END = 0;
    public static final int BOTTOM_START = 1;
    public static final int TOP_END = 2;
    public static final int TOP_START = 3;
    private static final int DEFAULT_MENU_POSITION = BOTTOM_END;

    private MenuListener menuListener;
    private NavigationMenu navigationMenu;
    private Map<Image, MenuItem> fabMenuItemMap;
    private Map<CardView, MenuItem> cardViewMenuItemMap;

    private DirectionalLayout menuItemsLayout;
    FloatingActionButton fab;
    private Component touchGuard = null;

    private int menuId;
    private int fabGravity;
    private Element fabDrawable;
    private ColorStateList fabDrawableTint;
    private ColorStateList fabBackgroundTint;
    private ColorStateList miniFabDrawableTint;
    private ColorStateList miniFabBackgroundTint;
    private int[] miniFabBackgroundTintArray;
    private ColorStateList miniFabTitleBackgroundTint;
    private boolean miniFabTitlesEnabled;
    private int miniFabTitleTextColor;
    private int[] miniFabTitleTextColorArray;
    private Element touchGuardDrawable;
    private boolean useTouchGuard;

    private boolean isAnimating;

    // Variable to hold whether the menu was open or not on config change
    private boolean shouldOpenMenu;

    public FabSpeedDial(Context context) {
        super(context);
        init(context, null);
    }

    public FabSpeedDial(Context context, AttrSet attrs) {
        super(context, attrs);
        try {
            init(context, attrs);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public FabSpeedDial(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttrSet attrs) {
        resolveCompulsoryAttributes(attrs);
        resolveOptionalAttributes(attrs);
        if (isGravityBottom()) {
            LayoutScatter.getInstance(context).parse(ResourceTable.Layout_fab_speed_dial_bottom, this, true);
        } else {
            LayoutScatter.getInstance(context).parse(ResourceTable.Layout_fab_speed_dial_top, this, true);
        }

        if (isGravityEnd()) {
            setAlignment(LayoutAlignment.END);
        }

        menuItemsLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_menu_items_layout);

        setOrientation(VERTICAL);

        newNavigationMenu();

        int menuItemCount = navigationMenu.size();
        fabMenuItemMap = new HashMap<>(menuItemCount);
        cardViewMenuItemMap = new HashMap<>(menuItemCount);

        setBindStateChangedListener(this);
        setKeyEventListener(this);
    }

    private void resolveCompulsoryAttributes(AttrSet attrs) {
        if (attrs == null) {
            return;
        }
        if (attrs.getAttr("fabMenu").isPresent()) {
            menuId = ResourceHelper.getResourceId(attrs, getContext(), "fabMenu", 0);
        } else {
            throw new RuntimeException("You must provide the id of the menu resource.");
        }

        if (attrs.getAttr("fabGravity").isPresent()) {
            fabGravity = parseFabGravity(ResourceHelper.getString(attrs, "fabGravity", "bottom_end"));
        } else {
            throw new RuntimeException("You must specify the gravity of the Fab.");
        }
    }

    private int parseFabGravity(String gravityStr) {
        switch (gravityStr) {
            case "bottom_end":
                return BOTTOM_END;
            case "bottom_start":
                return BOTTOM_START;
            case "top_end":
                return TOP_END;
            case "top_start":
                return TOP_START;
            default:
                return DEFAULT_MENU_POSITION;
        }
    }

    private void resolveOptionalAttributes(AttrSet attrs) {
        if (attrs == null) {
            return;
        }
        fabDrawable = ResourceHelper.getElement(attrs, "fabDrawable");
        if (fabDrawable == null) {
            fabDrawable = ElementScatter.getInstance(getContext()).parse(ResourceTable.Graphic_fab_add_clear_selector);
        }

        fabDrawableTint = getColorStateListByValue(ResourceHelper.getColorV(attrs, "fabDrawableTint",
                getContext().getColor(ResourceTable.Color_fab_drawable_tint)));

        if (attrs.getAttr("fabBackgroundTint").isPresent()) {
            fabBackgroundTint = getColorStateList(ResourceHelper.getColorV(attrs, "fabBackgroundTint", 0));
        }

        miniFabBackgroundTint = getColorStateListByValue(ResourceHelper.getColorV(attrs, "miniFabBackgroundTint",
                getContext().getColor(ResourceTable.Color_fab_background_tint)));

        if (attrs.getAttr("miniFabBackgroundTintList").isPresent()) {
            int miniFabBackgroundTintListId = ResourceHelper.getResourceId(attrs, getContext(),
                    "miniFabBackgroundTintList", 0);
            miniFabBackgroundTintArray = ResourceHelper.getIntArray(miniFabBackgroundTintListId, getContext());
        }

        miniFabDrawableTint = getColorStateListByValue(ResourceHelper.getColorV(attrs, "miniFabDrawableTint",
                getContext().getColor(ResourceTable.Color_mini_fab_drawable_tint)));

        miniFabTitleBackgroundTint = getColorStateListByValue(ResourceHelper.getColorV(attrs,
                "miniFabTitleBackgroundTint",
                getContext().getColor(ResourceTable.Color_mini_fab_title_background_tint)));
        getColorStateList(ResourceTable.Color_mini_fab_title_background_tint);

        miniFabTitlesEnabled = ResourceHelper.getBool(attrs, "miniFabTitlesEnabled", true);

        miniFabTitleTextColor = ResourceHelper.getColorV(attrs, "miniFabTitleTextColor",
                getContext().getColor(ResourceTable.Color_title_text_color));

        if (attrs.getAttr("miniFabTitleTextColorList").isPresent()) {
            int miniFabTitleTextColorListId = ResourceHelper.getResourceId(attrs, getContext(),
                    "miniFabTitleTextColorList", 0);
            miniFabTitleTextColorArray = ResourceHelper.getIntArray(miniFabTitleTextColorListId, getContext());
        }

        touchGuardDrawable = ResourceHelper.getElement(attrs, "touchGuardDrawable");

        useTouchGuard = ResourceHelper.getBool(attrs, "touchGuard", true);
    }

    @Override
    public void onComponentBoundToWindow(Component var1) {
        LayoutConfig layoutParams =
                new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                        ComponentContainer.LayoutConfig.MATCH_CONTENT);
        int coordinatorLayoutOffset = 0;
        try {
            coordinatorLayoutOffset =
                    (int) getContext().getResourceManager().getElement(ResourceTable.Float_coordinator_layout_offset).getFloat();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        if (fabGravity == BOTTOM_END || fabGravity == TOP_END) {
            layoutParams.setMargins(0, 0, coordinatorLayoutOffset, 0);
        } else {
            layoutParams.setMargins(coordinatorLayoutOffset, 0, 0, 0);
        }
        menuItemsLayout.setLayoutConfig(layoutParams);

        // Set up the client's FAB
        fab = (FloatingActionButton) findComponentById(ResourceTable.Id_fab);
        fab.setImageElement(fabDrawable);
        if (isGravityEnd() && fab.getLayoutConfig() instanceof DirectionalLayout.LayoutConfig) {
            ((LayoutConfig) fab.getLayoutConfig()).alignment = LayoutAlignment.END;
            fab.setLayoutConfig(fab.getLayoutConfig());
        }

        setImageTintList(fab, fabDrawableTint);

        if (fabBackgroundTint != null) {
            setBackgroundTintList(fab, fabBackgroundTint);
        }

        fab.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component v) {
                if (isAnimating) {
                    return;
                }

                if (isMenuOpen()) {
                    closeMenu();
                } else {
                    openMenu();
                }
            }
        });

        // Needed in order to intercept key events
        setFocusable(FOCUS_ENABLE);

        if (useTouchGuard) {
            ComponentParent parent = getComponentParent();

            touchGuard = new Component(getContext());
            touchGuard.setClickedListener(this);
            touchGuard.setVisibility(HIDE);

            if (touchGuardDrawable != null) {
                touchGuard.setBackground(touchGuardDrawable);
            }

            int index = getComponentParent().getChildIndex(this);
            if (parent instanceof StackLayout) {
                StackLayout frameLayout = (StackLayout) parent;
                frameLayout.addComponent(touchGuard, index);
                bringToFront();
            } else if (parent instanceof StackLayout) {
                StackLayout coordinatorLayout = (StackLayout) parent;
                coordinatorLayout.addComponent(touchGuard, index);
                bringToFront();
            } else if (parent instanceof DependentLayout) {
                DependentLayout relativeLayout = (DependentLayout) parent;
                relativeLayout.addComponent(touchGuard, index,
                        new DependentLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                                ComponentContainer.LayoutConfig.MATCH_PARENT));
                bringToFront();
            } else {
                HiLog.debug(LABEL, "touchGuard requires that the parent of this FabSpeedDialer be a StackLayout or " +
                        "DependentLayout");
            }
        }

        setClickedListener(this);

        if (shouldOpenMenu) {
            openMenu();
        }
    }

    public void onComponentUnboundFromWindow(Component var1) {

    }

    private void newNavigationMenu() {
        navigationMenu = new NavigationMenu(getContext());
        if (menuId <= 0) {
            return;
        }
        LayoutScatter.getInstance(getContext()).parse(menuId, navigationMenu, true);

        navigationMenu.setCallback(new NavigationMenu.Callback() {
            @Override
            public boolean onMenuItemSelected(NavigationMenu menu, MenuItem item) {
                return menuListener != null && menuListener.onMenuItemSelected(item);
            }
        });
    }

    @Override
    public void onClick(Component v) {
        fab.setSelected(false);
        removeFabMenuItems();

        if (menuListener != null) {
            if (v == this || v == touchGuard) {
                menuListener.onMenuClosed();
            } else if (v instanceof Image) {
                menuListener.onMenuItemSelected(fabMenuItemMap.get(v));
            } else if (v instanceof CardView) {
                menuListener.onMenuItemSelected(cardViewMenuItemMap.get(v));
            }
        } else {
            HiLog.debug(LABEL, "You haven't provided a MenuListener.");
        }
    }

    public void setMenuListener(MenuListener menuListener) {
        this.menuListener = menuListener;
    }

    public boolean isMenuOpen() {
        return menuItemsLayout.getChildCount() > 0;
    }

    public void openMenu() {
        if (!isBoundToWindow()) {
            return;
        }
        requestFocus();

        boolean showMenu = true;
        if (menuListener != null) {
            newNavigationMenu();
            showMenu = menuListener.onPrepareMenu(navigationMenu);
        }

        if (showMenu) {
            addMenuItems();
            fab.setSelected(true);
        } else {
            fab.setSelected(false);
        }
    }

    public void closeMenu() {
        if (!isBoundToWindow()) {
            return;
        }

        if (isMenuOpen()) {
            fab.setSelected(false);
            removeFabMenuItems();
            if (menuListener != null) {
                menuListener.onMenuClosed();
            }
        }
    }

    public void show() {
        if (!isBoundToWindow()) {
            return;
        }
        setVisibility(Component.VISIBLE);
        fab.show();
    }

    public void hide() {
        if (!isBoundToWindow()) {
            return;
        }

        if (isMenuOpen()) {
            closeMenu();
        }
        fab.hide();
    }

    private void addMenuItems() {
        menuItemsLayout.setAlpha(1f);
        for (int i = 0; i < navigationMenu.size(); i++) {
            MenuItem menuItem = navigationMenu.getItem(i);
            if (menuItem.isVisible()) {
                menuItemsLayout.addComponent(createFabMenuItem(menuItem));
            }
        }
        animateFabMenuItemsIn();
    }

    private Component createFabMenuItem(MenuItem menuItem) {
        ComponentContainer fabMenuItem = (ComponentContainer) LayoutScatter.getInstance(getContext())
                .parse(getMenuItemLayoutId(), this, false);
        Image miniFab = (Image) fabMenuItem.findComponentById(ResourceTable.Id_mini_fab);
        miniFab.setClipEnabled(false);
        CardView cardView = (CardView) fabMenuItem.findComponentById(ResourceTable.Id_card_view);
        cardView.setClipEnabled(false);
        Text titleView = (Text) fabMenuItem.findComponentById(ResourceTable.Id_title_view);

        fabMenuItemMap.put(miniFab, menuItem);
        cardViewMenuItemMap.put(cardView, menuItem);

        miniFab.setImageElement(menuItem.getIcon());
        miniFab.setClickedListener(this);
        cardView.setClickedListener(this);

        miniFab.setAlpha(0f);
        cardView.setAlpha(0f);

        final String title = menuItem.getTitle();
        if (!TextTool.isNullOrEmpty(title) && miniFabTitlesEnabled) {
            cardView.setCardBackgroundColor(miniFabTitleBackgroundTint.getDefaultColor());
            titleView.setText(title);
            titleView.setTextColor(new Color(miniFabTitleTextColor));

            if (miniFabTitleTextColorArray != null) {
                titleView.setTextColor(new Color(miniFabTitleTextColorArray[menuItem.getOrder()]));
            }
        } else {
            fabMenuItem.removeComponent(cardView);
        }

        setBackgroundTintList(miniFab, miniFabBackgroundTint);
        if (miniFabBackgroundTintArray != null) {
            setBackgroundTintList(miniFab, getColorStateListByValue(miniFabBackgroundTintArray[menuItem.getOrder()]));
        }

        setImageTintList(miniFab, miniFabDrawableTint);

        return fabMenuItem;
    }

    private void removeFabMenuItems() {
        if (touchGuard != null) {
            touchGuard.setVisibility(HIDE);
        }

        AnimatorProperty animatorProperty = menuItemsLayout.createAnimatorProperty()
                .setDuration(200)
                .alpha(0f)
                .setStateChangedListener(new Animator.StateChangedListener() {
                    @Override
                    public void onStart(Animator animator) {
                        isAnimating = true;
                    }

                    @Override
                    public void onStop(Animator animator) {

                    }

                    @Override
                    public void onCancel(Animator animator) {

                    }

                    @Override
                    public void onEnd(Animator animator) {
                        menuItemsLayout.removeAllComponents();
                        isAnimating = false;
                    }

                    @Override
                    public void onPause(Animator animator) {

                    }

                    @Override
                    public void onResume(Animator animator) {

                    }
                });
        animatorProperty.setCurve(new FastOutLinearInInterpolator());
        animatorProperty.start();
    }

    private void animateFabMenuItemsIn() {
        if (touchGuard != null) {
            touchGuard.setVisibility(VISIBLE);
        }

        final int count = menuItemsLayout.getChildCount();

        if (isGravityBottom()) {
            for (int i = count - 1; i >= 0; i--) {
                final Component fabMenuItem = menuItemsLayout.getComponentAt(i);
                animateViewIn(fabMenuItem.findComponentById(ResourceTable.Id_mini_fab), Math.abs(count - 1 - i));
                Component cardView = fabMenuItem.findComponentById(ResourceTable.Id_card_view);
                if (cardView != null) {
                    animateViewIn(cardView, Math.abs(count - 1 - i));
                }
            }
        } else {
            for (int i = 0; i < count; i++) {
                final Component fabMenuItem = menuItemsLayout.getComponentAt(i);
                animateViewIn(fabMenuItem.findComponentById(ResourceTable.Id_mini_fab), i);
                Component cardView = fabMenuItem.findComponentById(ResourceTable.Id_card_view);
                if (cardView != null) {
                    animateViewIn(cardView, i);
                }
            }
        }
    }

    private void animateViewIn(final Component view, int position) {
        final float offsetY = 0;//ResourceHelper.getFloat(ResourceTable.Float_keyline_1, getContext(), 0);

        view.setScaleX(0.25f);
        view.setScaleY(0.25f);
        view.setContentPositionY(view.getContentPositionY() + offsetY);

        AnimatorProperty animatorProperty = view.createAnimatorProperty()
                .setDuration(200)
                .scaleX(1f)
                .scaleY(1f)
                .moveByY(-offsetY)
                .alpha(1f)
                .setDelay(4 * position * VSYNC_RHYTHM)
                .setStateChangedListener(new Animator.StateChangedListener() {
                    @Override
                    public void onStart(Animator animator) {
                        isAnimating = true;
                    }

                    @Override
                    public void onStop(Animator animator) {

                    }

                    @Override
                    public void onCancel(Animator animator) {

                    }

                    @Override
                    public void onEnd(Animator animator) {
                        isAnimating = false;
                    }

                    @Override
                    public void onPause(Animator animator) {

                    }

                    @Override
                    public void onResume(Animator animator) {

                    }
                });
        animatorProperty.setCurve(new FastOutSlowInInterpolator());
        animatorProperty.start();
    }

    private int getMenuItemLayoutId() {
        if (isGravityEnd()) {
            return ResourceTable.Layout_fab_menu_item_end;
        } else {
            return ResourceTable.Layout_fab_menu_item_start;
        }
    }

    private boolean isGravityBottom() {
        return fabGravity == BOTTOM_END || fabGravity == BOTTOM_START;
    }

    private boolean isGravityEnd() {
        return fabGravity == BOTTOM_END || fabGravity == TOP_END;
    }

    private ColorStateList getColorStateList(int colorRes) {
        return getColorStateListByValue(getContext().getColor(colorRes));
    }

    private ColorStateList getColorStateListByValue(int colorValue) {
        int[][] states = new int[][]{

                new int[]{ComponentState.COMPONENT_STATE_DISABLED}, // disabled
                new int[]{-ComponentState.COMPONENT_STATE_CHECKED}, // unchecked
                new int[]{ComponentState.COMPONENT_STATE_PRESSED},  // pressed
                new int[]{ComponentState.COMPONENT_STATE_EMPTY} // enabled
        };
        int[] colors = new int[]{colorValue, colorValue, colorValue, colorValue};
        return new ColorStateList(states, colors);
    }

    @Override
    public boolean onKeyEvent(Component component, KeyEvent event) {
        if (isMenuOpen()
                && event.getKeyCode() == KeyEvent.KEY_BACK
                && !event.isKeyDown()
        ) {
            closeMenu();
            return true;
        }
        return false;
    }

    public static class ColorStateList {
        private int[][] states;
        private int[] colors;

        public ColorStateList(int[][] states, int[] colors) {
            this.states = states;
            this.colors = colors;
        }

        public int getDefaultColor() {
            return colors != null && colors.length > 0 ? colors[0] : 0;
        }

        @Override
        public String toString() {
            return "ColorStateList: [states: " + Arrays.deepToString(states) + ", colors: " + Arrays.toString(colors);
        }
    }

    private void setImageTintList(Image image, ColorStateList colorStateList) {
        Element element = image.getImageElement();

        if (element != null) {
            element.setStateColorList(colorStateList.states, colorStateList.colors);
            element.setStateColorMode(BlendMode.SRC_IN);
            image.setImageElement(element);
        }
    }

    private void setBackgroundTintList(Component component, ColorStateList colorStateList) {
        Element element = component.getBackgroundElement();
        if (element == null) {
            element = new ShapeElement();
            ((ShapeElement) element).setShape(ShapeElement.OVAL);
            ((ShapeElement) element).setRgbColor(RgbColor.fromArgbInt(colorStateList.getDefaultColor()));
        }
        component.setBackground(element);
    }

    private void bringToFront() {

    }
}