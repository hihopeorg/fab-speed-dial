/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.yavski.fabspeeddial;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

public class NavigationMenu extends StackLayout {
    private Callback callback;
    private int size;

    public NavigationMenu(Context context) {
        super(context);
    }

    public NavigationMenu(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public NavigationMenu(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    public int size() {
        return size;
    }

    public MenuItem getItem(int index) {
        if (size() == 0) {
            return null;
        }
        ComponentContainer container = (ComponentContainer) getComponentAt(0);
        MenuItem item = null;
        int size = -1;
        for (int i = 0; i < container.getChildCount(); i++) {
            if (container.getComponentAt(i) instanceof MenuItem) {
                if (++size == index) {
                    item = (MenuItem) container.getComponentAt(i);
                    break;
                }
            }
        }
        return item;
    }

    public MenuItem findItem(int id) {
        if (size() == 0) {
            return null;
        }
        ComponentContainer container = (ComponentContainer) getComponentAt(0);
        MenuItem item = null;
        for (int i = 0; i < container.getChildCount(); i++) {
            if (container.getComponentAt(i) instanceof MenuItem) {
                if (((MenuItem) container.getComponentAt(i)).getMenuId() == id) {
                    item = (MenuItem) container.getComponentAt(i);
                    break;
                }
            }
        }
        return item;
    }

    @Override
    public void addComponent(Component childComponent) {
        super.addComponent(childComponent);
        if (childComponent instanceof ComponentContainer) {
            ComponentContainer container = (ComponentContainer) childComponent;
            for (int i = 0; i < container.getChildCount(); i++) {
                Component item = container.getComponentAt(i);
                if (item instanceof MenuItem) {
                    ((MenuItem) item).setOrder(size++);
                    item.setClickedListener((c) -> {
                        if (callback != null) {
                            callback.onMenuItemSelected(NavigationMenu.this, (MenuItem) c);
                        }
                    });
                }
            }
        }
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    interface Callback {
        boolean onMenuItemSelected(NavigationMenu menu, MenuItem item);
    }

}
