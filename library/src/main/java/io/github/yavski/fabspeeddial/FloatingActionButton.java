/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.yavski.fabspeeddial;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

public class FloatingActionButton extends Image implements Component.LayoutRefreshedListener {
    private Element background;

    public FloatingActionButton(Context context) {
        super(context);
        init(null);
    }

    public FloatingActionButton(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    public FloatingActionButton(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    public void show() {
        setVisibility(VISIBLE);
    }

    public void hide() {
        setVisibility(HIDE);
    }

    private void init(AttrSet attrs) {
        if (getBackgroundElement() == null) {
            setBackground(new ShapeElement() {{
                setRgbColor(RgbColor.fromArgbInt(0xFFFF9800));
            }});
        }
        setClipEnabled(false);
        setLayoutRefreshedListener(this);
    }

    @Override
    public void setBackground(Element element) {
        background = element;
        addDrawTask((component, canvas) -> {
            getContext().getUITaskDispatcher().delayDispatch(() -> {
            }, 2000);


            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setColor(new Color(0xff777777));
            canvas.drawOval(new RectFloat(0, AttrHelper.vp2px(1, getContext()), getWidth(),
                    getHeight() + AttrHelper.vp2px(1, getContext())), paint);
            Path path = new Path();
            path.addCircle(new Point(getWidth() / 2, getHeight() / 2), Math.min(getWidth(), getHeight()) / 2,
                    Path.Direction.CLOCK_WISE);
            canvas.clipPath(path, Canvas.ClipOp.INTERSECT);
            if (getWidth() > 0 && getHeight() > 0) {
                element.setBounds(0, 0, getWidth(), getHeight());
                element.drawToCanvas(canvas);
            }

        }, DrawTask.BETWEEN_BACKGROUND_AND_CONTENT);
    }

    @Override
    public void onRefreshed(Component component) {
        invalidate();
    }

    @Override
    public void setImageElement(Element element) {
        int elementWidth = element.getWidth();
        int elementHeight = element.getHeight();
        if (elementWidth > 0 && elementHeight > 0) {
            double d = Math.sqrt(elementWidth * elementWidth + elementHeight * elementHeight);
            setPadding((int) (d - elementWidth) / 2, (int) (d - elementHeight) / 2, (int) (d - elementWidth) / 2,
                    (int) (d - elementHeight) / 2);
            setWidth((int) d);
            setHeight((int) d);
        }
        super.setImageElement(element);
        getContext().getUITaskDispatcher().asyncDispatch(this::invalidate);
    }
}
